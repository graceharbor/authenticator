module gitlab.com/graceharbor/authenticator

go 1.21.4

require (
	github.com/stretchr/testify v1.9.0
	gitlab.com/graceharbor/go-pco v0.0.0-20240625103946-add831c944c9
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/text v0.14.0 // indirect
)

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/afero v1.11.0
	github.com/spf13/cobra v1.8.1
)
