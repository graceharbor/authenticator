GHC Authenticator
=================

A `go` server to authenticate GHC members via
[PCO's OAuth](https://developer.planning.center/docs/#/overview/authentication).

It's endpoints are:

1) a custom one defined by the `completer` URL path in the config, which handles
   the return from PCO with the OAuth code.
2) `/auth` to actually do authentication based on a `sessionID` cookie
3) `/logout` to log a user out

The completer endpoint accepts a redirect from PCO with a `code` param, which is
used to retrieve an access token for the user. This token is used to create a
`sessionID` cookie for the client. The token, its `refresh_token`, and the
`sessionID` value are stored in a database of users.

The `/auth` endpoint returns a `200` if `sessionID` matches a valid user.
Occasionally, the `sessionID` is revoked and the user's `refresh_token` is used
to refresh with PCO. If it is refreshed, a new `sessionID` is created and sent
transparently to the user (via a redirect...sorry, but I couldn't figure out my
Caddy config to set the cookie *and* the `200`).

The `/logout` endpoint removes the `sessionID` from both the database and the
client. If a user *really* wants to logout, they'd need to
[revoke access](https://api.planningcenteronline.com/authorized_applications) to
the application.
