package internal

import (
	"net/url"
	"os"

	"gopkg.in/yaml.v3"
)

type ClientConfig struct {
	ID     string `yaml:"id"`
	Secret string `yaml:"secret"`
}

type Config struct {
	Org            int          `yaml:"org"`
	Client         ClientConfig `yaml:"client"`
	Port           int          `yaml:"port"`
	Redirect       string       `yaml:"redirect"`
	Completer      string       `yaml:"complete"`
	MembershipType []string     `yaml:"membership-type"`

	SoftExpires string `yaml:"soft-expires"`
	HardExpires string `yaml:"hard-expires"`

	pcoAuthEndpoint string
	pcoAPIRoot      string
}

func (c *Config) Load(path string) error {
	c.pcoAuthEndpoint = "https://api.planningcenteronline.com/oauth/token"
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	return yaml.NewDecoder(f).Decode(c)
}
func (c *Config) Path() string {
	u, err := url.Parse(c.Completer)
	if err != nil {
		return ""
	}
	return u.Path
}
