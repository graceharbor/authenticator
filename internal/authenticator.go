package internal

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/afero"
	"gitlab.com/graceharbor/go-pco"
	pco_typed "gitlab.com/graceharbor/go-pco/typed"
)

type Code string

type AuthenticatorError interface {
	error
	HTTPStatus() int
	Is(error) bool
}

type authenticatorError struct {
	msg    string
	status int
}

func (a *authenticatorError) Is(target error) bool {
	_, ok := target.(AuthenticatorError)
	return ok
}
func (a *authenticatorError) Error() string {
	return a.msg
}
func (a *authenticatorError) HTTPStatus() int {
	return a.status
}

type Authenticator interface {
	io.Closer
	CompleteAuthentication(context.Context, Code) (SessionID, error)
	CheckAuthentication(context.Context, SessionID) (SessionID, error)
	Logout(context.Context, SessionID)
}

type authenticator struct {
	conf Config
	db   Database

	conn *pco.Connection
}

func NewFlatFileAuthenticator(conf Config, fs afero.Fs, dbPath string) (Authenticator, error) {
	db, err := newFlatDatabase(fs, dbPath)
	if err != nil {
		return nil, err
	}
	var a = &authenticator{
		conf: conf,
		db:   db,
		conn: &pco.Connection{},
	}
	return a, nil
}

type pcoAuthResult struct {
	Token        string `json:"access_token"`
	Type         string `json:"token_type"`
	Expires      int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Scope        string
	Created      int `json:"created_at"`
}

const newTokenExpire = 6 * time.Hour

func createUser(result pcoAuthResult, id int) StoredUser {
	h := sha256.New()
	now := time.Now().Truncate(time.Second).UTC()
	fmt.Fprint(h, result.Token)
	fmt.Fprint(h, now.Format("2006-01-02T15:04:05"))
	sessionID := SessionID(hex.EncodeToString(h.Sum(nil)))
	return StoredUser{
		ID:           id,
		Token:        result.Token,
		RefreshToken: result.RefreshToken,
		SessionID:    sessionID,
		Expires:      now.Add(newTokenExpire),
	}
}

func (a *authenticator) isPersonInOurOrg(ctx context.Context, bearer string) (personID int, httpStatus int) {
	conn := pco.NewBearerConnection(bearer)
	if a.conf.pcoAPIRoot != "" {
		conn.SetAPIRoot(a.conf.pcoAPIRoot)
	}
	personData, err := conn.Get(ctx, pco.People, "/me")
	if err != nil {
		return 0, http.StatusNotExtended
	}
	var person pco_typed.Person
	err = personData.Datum.As(&person)
	if err != nil {
		return 0, http.StatusInternalServerError
	}

	if !slices.Contains(a.conf.MembershipType, person.Membership) {
		return 0, http.StatusForbidden
	}
	id, err := strconv.Atoi(personData.Datum.ID)
	if err != nil {
		return 0, http.StatusInternalServerError
	}
	if personData.Meta.Parent.Type == "Organization" && personData.Meta.Parent.ID == strconv.Itoa(a.conf.Org) {
		return id, http.StatusOK
	}
	return 0, http.StatusForbidden
}

func (a *authenticator) Close() error {
	return a.db.Close()
}
func (a *authenticator) callPcoAuth(ctx context.Context, data io.Reader) (result pcoAuthResult, err error) {
	req, err := http.NewRequest(http.MethodPost, a.conf.pcoAuthEndpoint, data)
	if err != nil {
		err = &authenticatorError{"failed to create PCO API request", http.StatusInternalServerError}
		return
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		err = &authenticatorError{"failed to authenticate with PCO", http.StatusForbidden}
		return
	}
	var responseBody []byte
	responseBody, err = io.ReadAll(res.Body)
	if err != nil {
		err = &authenticatorError{"failed to read PCO response", http.StatusInternalServerError}
		return
	}
	err = json.Unmarshal(responseBody, &result)
	if err != nil {
		err = &authenticatorError{"failed to unmarshal PCO response", http.StatusInternalServerError}
		return
	}
	return
}
func (a *authenticator) authenticateByCode(ctx context.Context, code Code) (id SessionID, err error) {
	return a.authenticateWith(ctx, strings.NewReader(fmt.Sprintf(`{
		"grant_type": "authorization_code",
		"code": "%s",
		"client_id": "%s",
		"client_secret": "%s",
		"redirect_uri": "%s"
	}`, code, a.conf.Client.ID, a.conf.Client.Secret, a.conf.Completer)))
}
func (a *authenticator) authenticateByRefresh(ctx context.Context, user StoredUser) (id SessionID, err error) {
	return a.authenticateWith(ctx, strings.NewReader(fmt.Sprintf(`{
		"grant_type": "refresh_token",
		"client_id": "%s",
		"client_secret": "%s",
		"refresh_token": "%s"
	}`, a.conf.Client.ID, a.conf.Client.Secret, user.RefreshToken)))
}
func (a *authenticator) authenticateWith(ctx context.Context, data io.Reader) (id SessionID, err error) {
	result, err := a.callPcoAuth(ctx, data)
	if err != nil {
		return
	}
	personID, status := a.isPersonInOurOrg(ctx, result.Token)
	if status != http.StatusOK {
		err = &authenticatorError{"user not allowed", http.StatusForbidden}
		return
	}
	if personID == 0 {
		err = &authenticatorError{"user ID is null", http.StatusInternalServerError}
		return
	}
	user := createUser(result, personID)
	err = a.db.CreateUser(user)
	if err != nil {
		err = &authenticatorError{"internal error 4cc83c7c-bfdb-4ee8-aa10-afaf9d800c70", http.StatusInternalServerError}
		return
	}
	return user.SessionID, nil
}

func (a *authenticator) CompleteAuthentication(ctx context.Context, code Code) (id SessionID, err error) {
	if matched, _ := regexp.MatchString("^[a-f0-9]+$", string(code)); !matched {
		err = fmt.Errorf("invalid code")
		return
	}
	return a.authenticateByCode(ctx, code)
}

func (a *authenticator) CheckAuthentication(ctx context.Context, id SessionID) (newID SessionID, err error) {
	dbUser, ok := a.db.GetUser(id)
	if !ok {
		fmt.Println("  user not found")
		err = &authenticatorError{"user not found", http.StatusForbidden}
		return
	}
	if dbUser.Expires.After(time.Now()) {
		fmt.Println("  token not expired")
		return id, nil
	}
	return a.authenticateByRefresh(ctx, dbUser)
}
func (a *authenticator) Logout(ctx context.Context, id SessionID) {
	a.db.RemoveUser(id)
}
