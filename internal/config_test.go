package internal_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/graceharbor/authenticator/internal"
)

func TestConfigPath(t *testing.T) {
	var conf = internal.Config{
		Completer: "http://example.com/completer",
	}
	assert.Equal(t, conf.Path(), "/completer")
}
