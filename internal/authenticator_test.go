package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAuthenticatorError(t *testing.T) {
	var err = &authenticatorError{"test", 1}
	var aerr AuthenticatorError
	require.True(t, errors.As(err, &aerr))
	assert.Equal(t, aerr.Error(), err.msg)
	assert.Equal(t, aerr.HTTPStatus(), err.status)
}

func getServer(personID, org int, membership, code, refresh string) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/people/v2/me":
			if r.Header.Get("Authorization") == "Bearer 1234" || r.Header.Get("Authorization") == "Bearer 1235" {
				w.WriteHeader(http.StatusOK)
				fmt.Fprintf(w, `
					{
						"data": {
							"id": "%d",
							"type": "Person",
							"attributes": {
								"membership": "%s"
							}
						},
						"meta": {
							"parent": {
								"id": "%d",
								"type": "Organization" 
							}
						}
					}
				`, personID, membership, org)
			}
		case "/auth":
			var authdata struct {
				GrantType    string `json:"grant_type"`
				Code         string `json:"code"`
				RefreshToken string `json:"refresh_token"`
			}
			data, _ := io.ReadAll(r.Body)
			json.Unmarshal(data, &authdata)
			switch {
			case authdata.GrantType == "authorization_code" && authdata.Code == code:
				w.WriteHeader(http.StatusOK)
				fmt.Fprint(w, `
				{
					"access_token": "1234",
					"token_type": "bearer",
					"expires_in": 7200,
					"refresh_token": "5678",
					"scope": "people",
					"created_at": 1469553476
					}
				`)
			case authdata.GrantType == "refresh_token" && authdata.RefreshToken == refresh:
				w.WriteHeader(http.StatusOK)
				fmt.Fprint(w, `
				{
					"access_token": "1235",
					"token_type": "bearer",
					"expires_in": 7200,
					"refresh_token": "5679",
					"scope": "people",
					"created_at": 1469553476
					}
				`)
			}
		}
	}))
}

func TestBasic(t *testing.T) {
	s := getServer(1, 1234, "ImaMember", "5678", "")
	conf := Config{
		Org:             1234,
		Client:          ClientConfig{},
		Port:            0,
		Redirect:        "",
		Completer:       "",
		MembershipType:  []string{"ImaMember"},
		pcoAuthEndpoint: s.URL + "/auth",
		pcoAPIRoot:      s.URL,
	}
	fs := afero.NewMemMapFs()
	afero.WriteFile(fs, "db.json", []byte("{}"), 0o755)
	a, err := NewFlatFileAuthenticator(conf, fs, "db.json")
	assert.NoError(t, err)
	sessionID, err := a.CompleteAuthentication(context.Background(), "5678")
	assert.NoError(t, err)
	assert.NoError(t, a.Close())

	dbi, err := newFlatDatabase(fs, "db.json")
	assert.NoError(t, err)
	defer dbi.Close()
	db := dbi.(*flatDatabase)
	assert.Len(t, db.Users, 1)
	assert.Len(t, db.userID2session, 1)
	assert.Contains(t, db.Users, sessionID)
	assert.Equal(t, db.Users[sessionID].ID, 1)
}

func TestRefresh(t *testing.T) {
	fs := afero.NewMemMapFs()
	afero.WriteFile(fs, "db.json", []byte("{}"), 0o755)

	dbi, err := newFlatDatabase(fs, "db.json")
	assert.NoError(t, err)
	user := createUser(pcoAuthResult{
		RefreshToken: "5678",
	}, 1)
	user.Expires = user.Expires.Add(-24 * time.Hour)
	assert.NoError(t, dbi.CreateUser(user))
	assert.NoError(t, dbi.Close())

	s := getServer(1, 1234, "ImaMember", "1", user.RefreshToken)
	conf := Config{
		Org:             1234,
		Client:          ClientConfig{},
		Port:            0,
		Redirect:        "",
		Completer:       "",
		MembershipType:  []string{"ImaMember"},
		pcoAuthEndpoint: s.URL + "/auth",
		pcoAPIRoot:      s.URL,
	}

	a, err := NewFlatFileAuthenticator(conf, fs, "db.json")
	assert.NoError(t, err)
	sessionID, _ := a.CheckAuthentication(context.Background(), user.SessionID)
	assert.NotEmpty(t, sessionID)
	a.Close()

	dbi, err = newFlatDatabase(fs, "db.json")
	assert.NoError(t, err)
	defer dbi.Close()
	db := dbi.(*flatDatabase)
	assert.Len(t, db.Users, 1)
	assert.Len(t, db.userID2session, 1)
	assert.Contains(t, db.Users, sessionID)
	assert.Equal(t, db.Users[sessionID].ID, 1)
	assert.Equal(t, db.Users[sessionID].Token, "1235")
}
