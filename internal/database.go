package internal

import (
	"encoding/json"
	"io"
	"sync"
	"time"

	"github.com/spf13/afero"
)

type Database interface {
	io.Closer
	GetUser(SessionID) (StoredUser, bool)
	CreateUser(StoredUser) error
	RemoveUser(SessionID)
}

type SessionID string

type StoredUser struct {
	ID           int       `json:"id"`
	Token        string    `json:"token"`
	RefreshToken string    `json:"refresh-token"`
	SessionID    SessionID `json:"session-id"`
	Expires      time.Time `json:"expires"`
}

type RWLocker interface {
	sync.Locker
	RLock()
	RUnlock()
}

type flatDatabase struct {
	Users map[SessionID]StoredUser `json:"users"`

	fs       afero.Fs
	fileName string

	wg       *sync.WaitGroup
	lock     RWLocker
	newUsers chan StoredUser

	userID2session map[int]SessionID
}

func (d *flatDatabase) Load() error {
	f, err := d.fs.Open(d.fileName)
	if err != nil {
		return err
	}
	defer f.Close()
	d.Users = make(map[SessionID]StoredUser)
	d.userID2session = make(map[int]SessionID)
	err = json.NewDecoder(f).Decode(d)
	if err != nil {
		return err
	}
	for _, u := range d.Users {
		d.userID2session[u.ID] = u.SessionID
	}
	return nil
}
func (d *flatDatabase) Close() error {
	close(d.newUsers)
	d.wg.Wait()
	return nil
}
func (d *flatDatabase) RemoveUser(id SessionID) {
	d.lock.Lock()
	defer d.lock.Unlock()
	d.removeUser(id)
}

// removeUser is the internal remove
// YOU MUST HAVE THE LOCK!
func (d *flatDatabase) removeUser(id SessionID) {
	user, ok := d.Users[id]
	if ok {
		delete(d.userID2session, user.ID)
		delete(d.Users, id)
	}
}
func (d *flatDatabase) GetUser(id SessionID) (StoredUser, bool) {
	d.lock.RLock()
	defer d.lock.RUnlock()
	u, ok := d.Users[id]
	return u, ok
}
func (d *flatDatabase) CreateUser(u StoredUser) error {
	d.newUsers <- u
	return nil
}

// dump the data to the file
// YOU MUST HAVE THE LOCK!
func (d *flatDatabase) dump() (err error) {
	f, err := d.fs.Create(d.fileName)
	if err != nil {
		return
	}
	defer func() {
		ferr := f.Close()
		if err == nil {
			err = ferr
		}
	}()
	e := json.NewEncoder(f)
	e.SetIndent("", "  ")
	err = e.Encode(d)
	return
}
func (d *flatDatabase) channelWatcher() {
	defer d.wg.Done()
	for newUser := range d.newUsers {
		func(u StoredUser) {
			d.lock.Lock()
			defer d.lock.Unlock()
			if othersession, ok := d.userID2session[u.ID]; ok {
				d.removeUser(othersession)
			}
			d.removeUser(u.SessionID)
			if u.ID == 0 {
				panic(u)
			}
			d.Users[u.SessionID] = u
			if d.Users[u.SessionID].ID == 0 {
				panic(u)
			}
			d.userID2session[u.ID] = u.SessionID
			d.dump()
		}(newUser)
	}
}

func newFlatDatabase(fs afero.Fs, path string) (Database, error) {
	ret := &flatDatabase{
		Users:          nil,
		wg:             &sync.WaitGroup{},
		lock:           &sync.RWMutex{},
		fs:             fs,
		fileName:       path,
		newUsers:       make(chan StoredUser),
		userID2session: map[int]SessionID{},
	}
	err := ret.Load()
	if err != nil {
		return nil, err
	}
	ret.wg.Add(1)
	go ret.channelWatcher()
	return ret, nil
}
