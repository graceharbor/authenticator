package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"gitlab.com/graceharbor/authenticator/internal"
)

var cmd = &cobra.Command{
	Use:               "ghc_auth",
	Long:              "authorization backend for GHC website",
	CompletionOptions: cobra.CompletionOptions{DisableDefaultCmd: true},
}

var (
	configPath   string
	databasePath string

	refresh string
)

func init() {

	cmd.AddCommand(
		&cobra.Command{
			Use:   "serve",
			Short: "run authentication server",
			Args:  cobra.NoArgs,
		},
		&cobra.Command{
			Use:   "acquire",
			Short: "acquire a new token",
			Args:  cobra.NoArgs,
		},
		&cobra.Command{
			Use:   "refresh",
			Short: "refresh a token",
			Args:  cobra.ExactArgs(1),
			Run:   func(cmd *cobra.Command, args []string) { refresh = args[0] },
		},
	)
	for _, c := range cmd.Commands() {
		c.Flags().StringVarP(&configPath, "config", "c", "", "path to configuration")
		c.Flags().StringVarP(&databasePath, "data", "d", "", "path to database")
	}
}

type server struct {
	conf internal.Config
	internal.Authenticator
}

type sessionStatus int

const (
	sessionNone    sessionStatus = 0
	sessionValid   sessionStatus = 1
	sessionInvalid sessionStatus = 2
	sessionExpired sessionStatus = 3
)

func readSession(r *http.Request) (internal.SessionID, sessionStatus) {
	sessionCookie, err := r.Cookie("sessionID")
	if err != nil {
		return "", sessionNone
	}
	if sessionCookie.Valid() != nil {
		return "", sessionInvalid
	}
	if sessionCookie.Expires.Before(time.Now()) {
		return internal.SessionID(sessionCookie.Value), sessionExpired
	}
	return internal.SessionID(sessionCookie.Value), sessionValid
}

func (a *server) logout(w http.ResponseWriter, r *http.Request) {
	sessionID, _ := readSession(r)
	a.Logout(r.Context(), sessionID)
	a.removeSessionCookie(w)
	http.Redirect(w, r, a.conf.Redirect, http.StatusTemporaryRedirect)
}

func (a *server) getDomain() string {
	uri, err := url.Parse(a.conf.Redirect)
	if err != nil {
		return ""
	}
	return uri.Hostname()
}
func (a *server) setCookie(w http.ResponseWriter, name, value string, offset time.Duration, remove bool) {
	cookie := &http.Cookie{
		Name:     name,
		Secure:   true,
		Path:     "/",
		HttpOnly: true,
		SameSite: http.SameSiteDefaultMode,
		Domain:   a.getDomain(),
	}
	if remove {
		cookie.Value = ""
		cookie.Expires = time.Time{}
		cookie.MaxAge = -1
	} else {
		expires := time.Now().Add(offset)
		cookie.Value = value
		cookie.Expires = expires
		cookie.MaxAge = int(time.Until(expires).Seconds())
	}
	http.SetCookie(w, cookie)
}
func (a *server) setSessionCookie(w http.ResponseWriter, id internal.SessionID) {
	d, _ := time.ParseDuration(a.conf.SoftExpires)
	a.setCookie(w, "sessionID", string(id), d, false)
}
func (a *server) removeSessionCookie(w http.ResponseWriter) {
	a.setCookie(w, "sessionID", "", 0, true)
}
func (a *server) setRedirectCookie(w http.ResponseWriter, to string) {
	a.setCookie(w, "login_redirect", to, 20*time.Minute, false)
}
func (a *server) removeRedirectCookie(w http.ResponseWriter) {
	a.setCookie(w, "login_redirect", "", 0, true)
}
func (a *server) getRedirectCookie(r *http.Request) string {
	destination := "/"
	redirection, err := r.Cookie("login_redirect")
	if err == nil {
		destination = redirection.Value
	}
	return destination
}
func (a *server) complete(w http.ResponseWriter, r *http.Request) {
	origUrl := getOriginalURL(r)
	query, err := url.ParseQuery(origUrl.RawQuery)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	redirect := query.Get("redirect")
	if redirect != "" {
		redirectUrl, err := url.QueryUnescape(redirect)
		if err == nil {
			http.Redirect(w, r, redirectUrl, http.StatusTemporaryRedirect)
			return
		}
	}
	code := internal.Code(query.Get("code"))

	sessionID, err := a.Authenticator.CompleteAuthentication(r.Context(), code)
	if err != nil {
		a.removeSessionCookie(w)
		http.Redirect(w, r, a.conf.Redirect, http.StatusTemporaryRedirect)
		return
	}

	a.setSessionCookie(w, sessionID)
	a.removeRedirectCookie(w)
	http.Redirect(w, r, a.getRedirectCookie(r), http.StatusTemporaryRedirect)
}

func getOriginalURL(r *http.Request) (ret url.URL) {
	origUri := r.Header.Get("X-Forwarded-Uri")
	origUrl, _ := url.Parse(origUri)
	if origUrl == nil {
		return
	}
	return *origUrl
}
func (a *server) auth(w http.ResponseWriter, r *http.Request) {
	origUri := getOriginalURL(r)

	sessionID, status := readSession(r)
	switch status {
	case sessionInvalid:
		a.removeSessionCookie(w)
		fallthrough
	case sessionNone:
		a.setRedirectCookie(w, origUri.String())
		http.Redirect(w, r, a.conf.Redirect, http.StatusTemporaryRedirect)
	case sessionValid:
		fallthrough
	case sessionExpired:
		sessionID_before := sessionID
		sessionID, err := a.Authenticator.CheckAuthentication(r.Context(), sessionID)
		if err != nil {
			a.removeSessionCookie(w)
			a.setRedirectCookie(w, origUri.String())
			http.Redirect(w, r, a.conf.Redirect, http.StatusTemporaryRedirect)
			return
		}
		a.setSessionCookie(w, sessionID)
		if sessionID == sessionID_before {
			w.WriteHeader(http.StatusOK)
		} else {
			http.Redirect(w, r, a.conf.Completer+"?redirect="+url.QueryEscape(origUri.String()), http.StatusTemporaryRedirect)
		}
	}
}

func main() {
	err := cmd.Execute()
	if err != nil {
		log.Fatal(err)
	}
	var conf internal.Config
	err = conf.Load(configPath)
	if err != nil {
		log.Fatal(err)
	}

	auth, err := internal.NewFlatFileAuthenticator(conf, afero.NewOsFs(), databasePath)
	if err != nil {
		log.Fatal(err)
	}

	var s = server{
		conf:          conf,
		Authenticator: auth,
	}

	completer, err := url.Parse(conf.Completer)
	if err != nil {
		log.Fatal(err)
		return
	}
	completerPath := "/" + strings.TrimPrefix(completer.Path, "/")

	http.HandleFunc(completerPath, s.complete)
	http.HandleFunc("/auth", s.auth)
	http.HandleFunc("/logout", s.logout)

	err = http.ListenAndServe(fmt.Sprintf(":%d", conf.Port), nil)
	auth.Close()
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Print("server closed")
	} else if err != nil {
		log.Fatal(err)
	}
}
